package cristian.com.accessphotogallery;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class PhotoActivity extends AppCompatActivity implements View.OnClickListener {

    int TAKE_PHOTO_CODE = 0;
    private ImageView im;
    private String newPhotoUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);

        if (savedInstanceState != null) {
            String s = savedInstanceState.getString("URI");
            if (s != null) {
                Uri uri = Uri.parse(s);
                Glide.with(this).load(uri).into(im);
            }
        }

        im = (ImageView) findViewById(R.id.image);
        Button b = (Button) findViewById(R.id.button);
        b.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button:
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                String file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + "/Camera/" + new Date().getTime() + ".jpg";
                File newFile = new File(file);
                try {
                    newFile.createNewFile();
                    Uri outputFileUri = Uri.fromFile(newFile);
                    newPhotoUri = outputFileUri.toString();

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    startActivityForResult(intent, TAKE_PHOTO_CODE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TAKE_PHOTO_CODE && resultCode == RESULT_OK) {
            Uri uri = Uri.parse(newPhotoUri);
            Glide.with(this).load(Uri.parse(newPhotoUri)).into(im);
            Intent mSI = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mSI.setData(uri);
            sendBroadcast(mSI);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }
}